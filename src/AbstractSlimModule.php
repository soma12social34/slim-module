<?php

namespace Somasocial\SlimModule;

use DI\Bridge\Slim\App;
use DI\ContainerBuilder;
use Slim\Route;

abstract class AbstractSlimModule extends App implements SlimModuleInterface
{
    /**
     * AbstractSlimModule constructor.
     */
    public function __construct()
    {
        parent::__construct();
        AbstractSlimModule::setRoutes($this, $this->getRoutesConfiguration());
    }

    /**
     * @param ContainerBuilder $builder
     */
    protected function configureContainer(ContainerBuilder $builder)
    {
        $dependencies = $this->getContainerConfiguration();
        $builder->addDefinitions($dependencies);

        if (!empty($dependencies['settings.containerCompilationDirectory'])) {
            $builder->enableCompilation($dependencies['settings.containerCompilationDirectory']);
        }
    }

    /**
     * Route configuration.
     *
     * Example:
     *
     * [
     *      '/users' => [
     *          'subRoutes' => [
     *              '[/]' => [
     *                  'httpMethod' => 'GET',
     *                  'callable' => [\ControllerA::class, 'read'],
     *                  'middleware => [
     *                      \MiddlewareA::class,
     *                      \MiddlewareB::class
     *                  ]
     *              ],
     *              '/{id}' => [
     *                  'httpMethod' => 'PUT',
     *                  'callable' => [\ControllerB::class, 'update']
     *              ]
     *          ],
     *          'middleware' => [
     *              \MiddlewareC::class,
     *              \MiddlewareD::class
     *          ]
     *      ]
     * ]
     *
     *
     * @param App   $app
     * @param array $routesConfiguration
     */
    public static function setRoutes(App $app, $routesConfiguration = [])
    {
        foreach ($routesConfiguration as $routePattern => $routeConfiguration) {
            if (!empty($routeConfiguration['subRoutes'])) {
                $route = $app->group($routePattern, function () use ($routeConfiguration) {
                    AbstractSlimModule::setRoutes($this, $routeConfiguration);
                });
            }

            if (!empty($routeConfiguration['httpMethod']) && !empty($routeConfiguration['callable'])) {
                $route = $app->map([$routeConfiguration['httpMethod']], $routePattern, $routeConfiguration['callable'])
                    ->setName($routeConfiguration['name']);
            }

            if (!empty($routeConfiguration['middleware']) && get_class($route) === Route::class) {
                foreach ($routeConfiguration['middleware'] as $middleware) {
                    $route->add($middleware);
                }
            }
        }
    }
}
