<?php

namespace Somasocial\SlimModule;

interface SlimModuleInterface
{
    /**
     * @return array
     */
    public function getContainerConfiguration();

    /**
     * @return array
     */
    public function getRoutesConfiguration();
}
